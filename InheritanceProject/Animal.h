#include <iostream>

class Animal {
public:
	virtual void Voice() {
		std::cout << "H-r-r-r!!!";
	}
};

class Mouse : public Animal {
public:
	void Voice() override {
		std::cout << "Peep!!!";
	}
};

class Cat : public Animal {
public:
	void Voice() override {
		std::cout << "Meow!!!";
	}
};

class Dog : public Animal {
public:
	void Voice() override {
		std::cout << "Woof!!!";
	}
};
