#include <iostream>
#include "Animal.h"

int main()
{
    const int SIZE = 3;
    Animal* animals[SIZE];
    animals[0] = new Mouse;
    animals[1] = new Cat;
    animals[2] = new Dog;

    for (int i = 0; i < SIZE; i++) {
        animals[i]->Voice();
        std::cout << std::endl;
    }

    for (int i = 0; i < SIZE; i++) {
        delete animals[i];
    }

    return 0;
}